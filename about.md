---
layout: article
titles:
  en      : &EN       About
  en-GB   : *EN
  en-US   : *EN
  en-CA   : *EN
  en-AU   : *EN
  zh-Hans : &ZH_HANS  关于
  zh      : *ZH_HANS
  zh-CN   : *ZH_HANS
  zh-SG   : *ZH_HANS
  zh-Hant : &ZH_HANT  關於
  zh-TW   : *ZH_HANT
  zh-HK   : *ZH_HANT
  ko      : &KO       소개
  ko-KR   : *KO
key: page-about
---

<div class="card">
  <div class="card__content">
    <div class="card__header">
      <h4>Author</h4>
    </div>
    <p>I am a person</p>
  </div>
</div>

Hello darkness my old friend, I've come to talk to you again.